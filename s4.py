import os
import random
import random
import string

from werkzeug.wrappers import Request, Response
from werkzeug import secure_filename


def get_random_string(length, chars=string.ascii_lowercase+string.digits):
    return "".join([random.choice(chars) for i in xrange(length)])

def get_wsgi(static_root, static_url, drives):
    @Request.application
    def application(request):
        try:
            f = request.files.get("file")
        except IOError:
            return Response("Can't write temp file or request timeout", status=500)
        if not f:
            return Response("No file uploaded", status=400)

        randomhash = get_random_string(17)
        relative_file_dir = "%s/%s/%s/%s/" % (random.choice(drives),
                                              randomhash[0:3],
                                              randomhash[3:6],
                                              randomhash[6:9], )
        relative_file_path = "%s%s_%s" % (relative_file_dir,
                                          randomhash[9:17],
                                          secure_filename(f.filename), )
        file_dir = "%s/%s" % (static_root, relative_file_dir)
        file_path = "%s/%s" % (static_root, relative_file_path)
        file_url = "%s%s" % (static_url, relative_file_path)
        try:
            os.makedirs(file_dir)
            f.save(file_path)
        except OSError:
            return Response("Can't write file", status=500)
        return Response(file_url, status=200)
    return application
