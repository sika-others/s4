#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = 's4',
    version = '1.0.0',
    download = 'http://github.com/sikaondrej/s4',
    download_url = 'http://github.com/sikaondrej/s4',
    license = 'MIT',
    description = 'Lighweight & robust file uploader',
    author = 'Ondrej Sika',
    author_email = 'dev@ondrejsika.com',
    py_modules = ['s4', ],
    install_requires = ['werkzeug', ],
)
